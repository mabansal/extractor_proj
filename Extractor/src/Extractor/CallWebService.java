package Extractor;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class CallWebService {
	
public String getXmlFile(String inputFile)  throws ClientProtocolException, IOException{
		
		String request = "http://176.9.30.146:8080/sentiment?input_file="+inputFile+"&output_format=xml";
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(request);
		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		if (entity != null) {
		InputStream instream = entity.getContent();
		int l;
		byte[] tmp = new byte[2048];
		while ((l = instream.read(tmp)) != -1) {
			System.out.println(instream);
		}
		String retVal = new String(tmp);
		System.out.println(retVal);
		String output=retVal.split(",")[1].split(":")[1];
		String file = output.substring(2,output.lastIndexOf("\""));
		return file;
		}
		return "";
	}
}
