package Extractor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.xml.sax.SAXException;

public  class ExtractorReducer extends Reducer<Text, Text, Text, Text> {
	private MultipleOutputs mos;
	private ArrayList<ArrayList<HashMap<String, HashMap<String, String>>>> posts = new ArrayList<ArrayList<HashMap<String,HashMap<String,String>>>>();
	
	public void reduce(Text key, Iterable<Text> value,
			Context context) throws IOException, InterruptedException {
		
		System.out.println(key);
		String filePath = "";
		for (Text val : value) {
			System.out.println(val);
			filePath = val.toString();
		}
		CallWebService web = new CallWebService();
		FileSystem fs = FileSystem.get(context.getConfiguration());
		fs.copyToLocalFile(new Path(filePath), new Path(filePath));
		String strFile = web.getXmlFile(filePath);
		
		ParseXML parse = new ParseXML();
		try {
			parse.readXML(strFile,posts);
		//	parse.readXML("/Users/mabansal/privateproj/tmpyS7VuQ",posts);
			for(int i = 0;i<posts.size();i++){
				ArrayList<HashMap<String, HashMap<String, String>>> post = posts.get(i);
				String str_post_table = "";
				String str_post_data  = "";
				String str_post_sentiments = "";
				String str_post_scores = "";
				for(int j=0;j<post.size();j++){
					if(post.get(j).containsKey("posts_table")){
						str_post_table = getPostString(post.get(j).get("posts_table"));
					}else if(post.get(j).containsKey("post_data")){
						str_post_data = getPostString(post.get(j).get("post_data"));
					}else if(post.get(j).containsKey("post_sentiments")){
						str_post_sentiments = getPostString(post.get(j).get("post_sentiments"));
					}else if(post.get(j).containsKey("post_scores")){
						str_post_scores = getPostString(post.get(j).get("post_scores"));
					}
				}
				mos.write("poststable", "", new Text(str_post_table));
				mos.write("postdata", "", new Text(str_post_data));
				mos.write("postsentiments", "", new Text(str_post_sentiments));
				mos.write("postscores", "", new Text(str_post_scores));
			}
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		

	}
	
	private String getPostString(HashMap<String, String> hash){
		String str = new String();
		
		for(String key : hash.keySet()){
			str = str + hash.get(key);
			str = str + ",";
		}
		str = str.substring(0, str.length()-1);
		return str;
	}
	
	public void setup(Context context) {
		mos = new MultipleOutputs(context);
	}
	
	public void cleanup(Context context) throws IOException {
		 try {
			mos.close();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
