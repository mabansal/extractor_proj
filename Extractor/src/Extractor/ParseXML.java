package Extractor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;


public class ParseXML {
	HashMap<String, String> hashPostAttr;
	
	public String getXmlFile()  throws ClientProtocolException, IOException{
		
		String request = "http://localhost:8080/sentiment?input_file=/home/mruser/sampleoutput/small_input.csv&output_format=xml";
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(request);
		//HttpPost httppost = new HttpPost(request);
		//httppost.getParams().setParameter("input_file", "/home/mruser/sampleoutput/small_input.csv");
		//httppost.getParams().setParameter("output_format", "xml");
		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		if (entity != null) {
		InputStream instream = entity.getContent();
		int l;
		byte[] tmp = new byte[2048];
		while ((l = instream.read(tmp)) != -1) {
			System.out.println(instream);
		}
		System.out.println(new String(tmp));
		}
		return "";
	}
	
	public void execWebService() throws IOException{
		String command = "curl http://176.9.30.146:8080/sentiment?input_file=/home/mruser/sampleoutput/small_input.csv&output_format=xml";
		System.out.println(command);
		Process p = Runtime.getRuntime().exec(command,getEnv());
		InputStream instream = p.getInputStream();
		int l;
		byte[] tmp = new byte[2048];
		while ((l = instream.read(tmp)) != -1) {
		}
		System.out.println(new String(tmp));
	}
	
	public String[] getEnv(){
	        Map<String, String> env = System.getenv();
	        String[] envStr = new String[env.size()];
	        int i =0;
	        for (String envName : env.keySet()) {
	        	envStr[i++] = String.format("%s=%s", envName, env.get(envName));
	        }
	        for(i =0;i<envStr.length;i++){
	        	System.out.println(envStr[i]);
	        }
	        return envStr;
	    }
	
	public void readXML(String xmlPath,ArrayList<ArrayList<HashMap<String, HashMap<String, String>>>> posts) throws ParserConfigurationException, SAXException, IOException{
		try {
		
		File fXmlFile = new File(xmlPath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		NodeList nList = doc.getElementsByTagName("post");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			
		   // Create multiple hashmaps for tables
		   ArrayList<HashMap<String,HashMap<String, String>>> arr = new ArrayList<HashMap<String,HashMap<String,String>>>();	
		   HashMap<String, String> posts_table = create_posts_table();
		   HashMap<String, String> post_sentiments = create_post_sentiments();
		   HashMap<String, String> post_scores = create_post_scores();
		   HashMap<String, String> post_data = create_post_data();
		   HashMap<String,HashMap<String, String>> h1 = new HashMap<String, HashMap<String,String>>();
		   h1.put("posts_table", posts_table);
		   arr.add(h1);
		   
		   HashMap<String,HashMap<String, String>> h2 = new HashMap<String, HashMap<String,String>>();
		   h2.put("post_sentiments", post_sentiments);
		   arr.add(h2);
		   
		   HashMap<String,HashMap<String, String>> h3 = new HashMap<String, HashMap<String,String>>();
		   h3.put("post_scores", post_scores);
		   arr.add(h3);
		   
		   HashMap<String,HashMap<String, String>> h4 = new HashMap<String, HashMap<String,String>>();
		   h4.put("post_data", post_data);
		   arr.add(h4);
		   
		   
		   Node nNodePost = nList.item(temp);
		   HashMap<String,String> hashPostAttr = getAttributes(nNodePost);
		   HashMap<String,String> hashProductAttr = null;
		   HashMap<String,String> hashSentimentAttr = null;
		   HashMap<String,String> hashWordAttr = null;
		   NodeList nListProducts = nNodePost.getChildNodes();
		   NodeList nListProduct = nListProducts.item(0).getChildNodes();
		   for (int i =0;i<nListProduct.getLength();i++){
			   Node product = nListProduct.item(i);
			   hashProductAttr = getAttributes(product);
			   NodeList nListSentiments = product.getChildNodes();
			   NodeList nListSentiment = nListSentiments.item(0).getChildNodes();
			   for (int j =0;j<nListSentiment.getLength();j++){
				   Node sentiment = nListSentiment.item(j);
				   hashSentimentAttr = getAttributes(sentiment);
				   NodeList nListWords = sentiment.getChildNodes();
				   NodeList nListWord = nListWords.item(0).getChildNodes();
				   for (int k =0;k<nListSentiment.getLength();k++){
					   Node word = nListWord.item(k);
					   hashWordAttr = getAttributes(word);
				   }
			   }
		   }
		   MappingtoDB(arr,hashPostAttr,hashProductAttr,hashSentimentAttr,hashWordAttr);
		   posts.add(arr);
		}
	  }catch (Exception e) {
		e.printStackTrace();
	  }
	}
	
	private void MappingtoDB(ArrayList<HashMap<String,HashMap<String, String>>> arr,
			HashMap<String,String> hashPostAttr,
			HashMap<String,String> hashProductAttr,
			HashMap<String,String> hashSentimentAttr,
			HashMap<String,String> hashWordAttr){
		for (int i =0;i<arr.size();i++){
			if(arr.get(i).containsKey("posts_table")){
				if(hashPostAttr.containsKey("data")){
					arr.get(i).get("posts_table").put("data", hashPostAttr.get("data"));
				}
				if(hashPostAttr.containsKey("title")){
					arr.get(i).get("posts_table").put("title", hashPostAttr.get("title"));
				}
				if(hashPostAttr.containsKey("id")){
					arr.get(i).get("posts_table").put("id", hashPostAttr.get("id"));
				}
				if(hashPostAttr.containsKey("language")){
					arr.get(i).get("posts_table").put("language_id", hashPostAttr.get("language"));
				}
				if(hashPostAttr.containsKey("source")){
					arr.get(i).get("posts_table").put("source", hashPostAttr.get("source"));
				}
				if(hashPostAttr.containsKey("posted_date")){
					arr.get(i).get("posts_table").put("posted_date", hashPostAttr.get("posted_date"));
				}
				
			}
			if(arr.get(i).containsKey("post_data")){
				if(hashPostAttr.containsKey("rank")){
					arr.get(i).get("post_data").put("rank", hashPostAttr.get("rank"));
				}
				if(hashProductAttr.containsKey("id")){
					arr.get(i).get("post_data").put("product_id", hashProductAttr.get("id"));
				}
				
			}
			if(arr.get(i).containsKey("post_sentiments")){
				if(hashProductAttr.containsKey("id")){
					arr.get(i).get("post_sentiments").put("product_id", hashProductAttr.get("id"));
				}
				if(hashSentimentAttr.containsKey("feature_id")){
					arr.get(i).get("post_sentiments").put("feature_id", hashSentimentAttr.get("feature_id"));
				}
				if(hashSentimentAttr.containsKey("negative_probability")){
					arr.get(i).get("post_sentiments").put("negative_prob", hashSentimentAttr.get("negative_probability"));
				}
				if(hashSentimentAttr.containsKey("positive_probability")){
					arr.get(i).get("post_sentiments").put("positive_prob", hashSentimentAttr.get("positive_probability"));
				}
				if(hashSentimentAttr.containsKey("snippet")){
					arr.get(i).get("post_sentiments").put("snippet", hashSentimentAttr.get("snippet"));
				}
				if(hashSentimentAttr.containsKey("sentiment_id")){
					arr.get(i).get("post_sentiments").put("sentiment_id", hashSentimentAttr.get("sentiment_id"));
				}
			}
			if(arr.get(i).containsKey("post_scores")){
				if(hashProductAttr.containsKey("id")){
					arr.get(i).get("post_scores").put("product_id", hashProductAttr.get("id"));
				}
				if(hashProductAttr.containsKey("weighted_score")){
					arr.get(i).get("post_scores").put("weighted_score", hashProductAttr.get("weighted_score"));
				}
				if(hashProductAttr.containsKey("unweighted_score")){
					arr.get(i).get("post_scores").put("unweighted_score", hashProductAttr.get("unweighted_score"));
				}
			}
			
		}
	}
	
	private HashMap<String, String> create_posts_table(){
		HashMap<String, String> posts_tables = new HashMap<String, String>();
		posts_tables.put("old_id", "");
		posts_tables.put("old_connector_instance_id", "");
		posts_tables.put("old_workspace_id", "");
		posts_tables.put("previous_version_id", "");
		posts_tables.put("level", "");
		posts_tables.put("posted_date", "");
		posts_tables.put("pickup_date", "");
		posts_tables.put("last_updated_time", "");
		posts_tables.put("version_number", "");
		posts_tables.put("title", "");
		posts_tables.put("data", "");
		posts_tables.put("uri", "");
		posts_tables.put("source", "");
		posts_tables.put("source_type", "");
		posts_tables.put("entity", "");
		posts_tables.put("path", "");
		posts_tables.put("parent_path", "");
		posts_tables.put("uuid", "");
		posts_tables.put("versioned", "");
		posts_tables.put("active_status", "");
		posts_tables.put("delete_status", "");
		posts_tables.put("orig_data", "");
		posts_tables.put("is_latest", "");
		posts_tables.put("old_language_id", "");
		posts_tables.put("translated_data", "");
		posts_tables.put("referenced_posts", "");
		posts_tables.put("translated_title", "");
		posts_tables.put("id", "");
		posts_tables.put("connector_instance_id", "");
		posts_tables.put("language_id", "");
		posts_tables.put("workspace_id", "");
		posts_tables.put("_tid", "");
		posts_tables.put("relevancy", "");
		
		return posts_tables;
	}
	
	private HashMap<String, String> create_post_data(){
		HashMap<String, String> posts_sentiments = new HashMap<String, String>();
		posts_sentiments.put("old_id", "");
		posts_sentiments.put("old_post_id", "");
		posts_sentiments.put("new_content", "");
		posts_sentiments.put("rank", "");
		posts_sentiments.put("old_product_id", "");
		posts_sentiments.put("id", "");
		posts_sentiments.put("post_id", "");
		posts_sentiments.put("product_id", "");
		posts_sentiments.put("_tid", "");
		return posts_sentiments;
	}
	
	private HashMap<String, String> create_post_sentiments(){
		HashMap<String, String> post_scores = new HashMap<String, String>();
		post_scores.put("old_id", "");
		post_scores.put("old_post_id", "");
		post_scores.put("old_product_id", "");
		post_scores.put("old_feature_id", "");
		post_scores.put("snippet", "");
		post_scores.put("old_sentiment_id", "");
		post_scores.put("positive_prob", "");
		post_scores.put("negative_prob", "");
		post_scores.put("curated", "");
		post_scores.put("id", "");
		post_scores.put("feature_id", "");
		post_scores.put("post_id", "");
		post_scores.put("product_id", "");
		post_scores.put("sentiment_id", "");
		post_scores.put("_tid", "");
		post_scores.put("negative_words", "");
		post_scores.put("positive_words", "");
		post_scores.put("bit_array", "");
		return post_scores;
	}
	
	private HashMap<String, String> create_post_scores(){
		HashMap<String, String> post_data = new HashMap<String, String>();
		post_data.put("old_post_id", "");
		post_data.put("old_product_id", "");
		post_data.put("unweighted_score", "");
		post_data.put("weighted_score", "");
		post_data.put("old_id", "");
		post_data.put("id", "");
		post_data.put("product_id", "");
		post_data.put("post_id", "");
		post_data.put("_tid", "");
		return post_data;
	}
	
	private HashMap<String,String> getAttributes(Node node){
		HashMap<String, String> hashAttr = new HashMap<String, String>();
		if(node != null && node.getAttributes() != null){
			for(int i =0;i< node.getAttributes().getLength();i++ ){
				hashAttr.put(node.getAttributes().item(i).getNodeName(), node.getAttributes().item(i).getNodeValue());
			}
		}
		return hashAttr;    
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException{
		String xmlPath = "/Users/mabansal/tmpe6M66V.xml";
		ParseXML p = new ParseXML();
		//p.readXML(xmlPath);
		//p.getXmlFile();
		p.getXmlFile();
		p.execWebService();
	}	
	
}
