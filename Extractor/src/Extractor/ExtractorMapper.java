package Extractor;

import java.io.IOException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*; 
//import org.apache.commons.lang.StringEscapeUtils;

public class ExtractorMapper extends Mapper<LongWritable, Text, Text, Text> {
	private FSDataOutputStream out;
	private FileSystem fs;
	private Path filenamePath;
	private Context context;
	private String fileName;
	public void map(LongWritable offset, Text value,
			Context context)
			throws IOException {
		this.context = context;
		out.writeBytes(UnicodeString.convert(value.toString()));
		out.writeBytes("\n");		
	} 
	
	public void setup(Context  context){
		try {
			fs = FileSystem.get(context.getConfiguration());
			String taskAttemptId = context.getTaskAttemptID().getTaskID().toString();
			String InputFile = context.getJobID().toString();
			fileName = InputFile+"_"+taskAttemptId;
			String theFilename = "/tmp/" + fileName;
			filenamePath = new Path(theFilename);
			if (fs.exists(filenamePath)) {
				// remove the file first
				fs.delete(filenamePath);
			}
			out = fs.create(filenamePath);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanup(Context context) throws InterruptedException{
		try {
			out.close();
			this.context.write(new Text(fileName),new Text(filenamePath.toString()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}